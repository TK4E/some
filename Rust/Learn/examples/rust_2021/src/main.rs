#![feature(format_args_capture)]
fn format_args_capture() {
    let a = b'c';
    let b = r#"lssss"#;

    println!("{a}");
    println!("{b}");
}

fn main() {
    format_args_capture();
}
