use std::{fs::File, io::Read, io};

fn main() {
}



fn rand_dev_num() -> io::Result<u8> {
    let mut f = File::open("/dev/urandom").unwrap();
    let mut buf = [0_u8; 1];

    f.read_exact(&mut buf)?;

    Ok(buf[0].into())
}
