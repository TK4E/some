
fn get_first_char(text: &str) -> char {
    text.chars().next().unwrap_or_default()
}

fn char_to_usize(text: char) -> usize {
    text as usize
}

fn text_regex(){

    use regex::Regex;
    let re = Regex::new(r"^\d{4}-\d{2}-\d{2}$").unwrap();
    assert!(re.is_match("2014-01-01"));
}
