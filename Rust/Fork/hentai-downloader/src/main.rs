#[macro_use]
extern crate clap;
extern crate reqwest;
extern crate scraper;

use scraper::{Html, Selector};

mod handler;
mod manga;
mod parser;

use crate::parser::Cli;

use clap::App;
use handler::Handler;
use manga::Manga;
use std::fs;
use std::path::Path;
use threadpool::ThreadPool;

fn get_title(url: &str) -> String {
    let mut resp = reqwest::get(url).unwrap();
    assert!(resp.status().is_success());
    let body = resp.text().unwrap();
    let fragment = Html::parse_fragment(&body);

    let selector = Selector::parse("h1").unwrap();

    let h1 = fragment.select(&selector).next().unwrap();
    let text = h1.text().collect::<Vec<_>>();
    let nx = format!("{}", text[0]);
    nx
}

pub fn get_url(matches: &clap::ArgMatches) -> String {
    let url: String = matches
        .value_of("url")
        .expect("Should provide the url.")
        .parse::<String>()
        .expect("Incorrect url.");
    url
}

fn main() -> Result<(), Box<std::error::Error>> {
    let yaml = load_yaml!("cli/cli-en.yml");
    let matches = App::from_yaml(yaml).get_matches();

    let Cli { url, cookie } = parser::parse_cli(&matches);

    let host = url.host().unwrap().to_string();

    let h = Handler::new(&host, &cookie);
    let m = Manga::new(&h, &url);

    println!("Collect Download information");
    let download_urls = m.get_download_urls(&h);

    // starting download
    let pool = ThreadPool::new(16);
    //let path = format!("tmp{}", m.number);

    let args: Vec<_> = std::env::args().collect();

    //let path = get_title(&args[2]);

    let mut path_url = String::new();

    if let Some(u) = matches.value_of("vals") {
        let path_url = format!("{}", u);
    }
    println!("{}", path_url);

    //println!("{:?}", path_url);

    let path = get_title(get_url(&matches).as_str());

    if !Path::new(&path).exists() {
        fs::create_dir(&path)?;
    }
    for target in download_urls {
        let path = path.clone();
        pool.execute(move || {
            // retry download 3 times if timeout
            // it will happen when the file is too slow in loading
            let times = 3;
            for _ in 0..times {
                match Handler::download(&target, &path) {
                    Ok(_) => break,
                    Err(ref e) if e.is_timeout() => {
                        println!("Download Timeout, retry download {}", target);
                    }
                    Err(ref e) => panic!(
                        "Something wrong when download file from url {}\nWith Error: {}",
                        target, e
                    ),
                }
            }
        });
    }

    pool.join();

    Ok(())
}
