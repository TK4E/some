use ansi_term::Colour::RGB;

fn main() {
    let im = image::open("0.jpg").unwrap();
    // let rgb = im.as_flat_samples_u16();
    // println!("{:?}", &im.color());

    let n = &im.into_bytes()[0..8200004];
    //let n = &im.into_bytes();
    let _n_len = n.len();

    for chunk in n.chunks(3).skip(2000000) {
        println!("{}", RGB(chunk[0], chunk[1], chunk[2]).paint("████"));
    }
}
