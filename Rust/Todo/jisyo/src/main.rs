use serde::Deserialize;
use std::{env, error::Error, ffi::OsString, process};

#[derive(Deserialize)]
struct Jisyo {
    ja: String,
    zh: String,
}

fn run() -> Result<(), Box<dyn Error>> {
    let path = get_first_arg()?;

    let mut rdr = csv::ReaderBuilder::new()
        .has_headers(false)
        .delimiter(b'\t')
        .double_quote(false)
        .escape(Some(b'\\'))
        .flexible(true)
        .comment(Some(b'#'))
        .from_path(path)?;

    for result in rdr.deserialize() {
        let record: Jisyo = result?;
        println!("{} \t---\t {}", record.ja, record.zh,);
    }

    Ok(())
}

/// Returns the first positional argument sent to this process. If there are no
/// positional arguments, then this returns an error.
fn get_first_arg() -> Result<OsString, Box<dyn Error>> {
    match env::args_os().nth(1) {
        None => Err(From::from("expected 1 argument, but got none")),
        Some(file_path) => Ok(file_path),
    }
}

fn main() {
    if let Err(err) = run() {
        println!("{}", err);
        process::exit(1);
    }
}
