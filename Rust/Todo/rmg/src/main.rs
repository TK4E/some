use rmg::utils::{display, error};
use std::path::Path;

const MAX_CACHE: usize = 100;

fn main() {
    let args: Vec<_> = std::env::args().collect();

    if args.len() < 2 {
        error::exit("cargo run -- tests/file/png.tar")
    }
    display::display_window(Path::new(&*args[1]));
}
