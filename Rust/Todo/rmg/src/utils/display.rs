use crate::{
    archive::{tar, zip},
    utils::{error, get},
};

use sdl2::{
    event::Event,
    image::LoadTexture,
    keyboard::Keycode,
    rect::Rect,
    render::{Texture, WindowCanvas},
};
use std::{
    fs,
    path::{Path, PathBuf},
    time::Duration,
};
use tempfile::TempDir;

const PLAYER_MOVEMENT_SPEED: i32 = 10;
const MAX_DISPLAY_IMG: u32 = 4;

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Move {
    Up,
    Down,
    Left,
    Right,
    Stop,
}

#[derive(Debug)]
pub struct Player {
    pub rects: Vec<(Rect, Rect)>,
    pub direction: Move,
    pub speed: i32,
    pub len: usize,
    pub max_len: usize,
    pub screen_size: (u32, u32),
}

pub fn display_window(path: &Path) {
    let tmp_dir = TempDir::new().unwrap().into_path();

    match get::get_filetype(path).as_ref() {
        "tar" => {
            if tar::extract(path, &tmp_dir).is_err() {
                error::exit("#26: tar::extract()")
            }
        }

        "zip" => {
            if zip::extract(path, &tmp_dir).is_err() {
                error::exit("#32: zip::extract()")
            }
        }
        _ => {}
    }

    if display_image(&(get::get_file_list(&tmp_dir))).is_err() {
        eprintln!("{:#?}", &(get::get_file_list(&tmp_dir)));
        error::exit("#2: display_image()")
    };

    if fs::remove_dir_all(tmp_dir).is_err() {
        error::exit("#3: remove_dir_all()")
    };
}

pub fn display_image(file_iter: &[PathBuf]) -> Result<(), String> {
    let sdl_context = sdl2::init()?;
    let screen = &sdl_context.video().unwrap().current_display_mode(0)?;

    let screen_size = (screen.w as u32, screen.h as u32);
    let video_subsystem = sdl_context.video()?;

    let window = video_subsystem
        .window("title", screen_size.0, screen_size.1)
        //.fullscreen_desktop()
        .position(0, 0)
        .build()
        .map_err(|e| e.to_string())?;

    let mut canvas = window
        .into_canvas()
        .software()
        .build()
        .map_err(|e| e.to_string())?;

    let mut event_pump = sdl_context.event_pump()?;
    let texture_creator = canvas.texture_creator();

    let mut run = true;
    let mut rects: Vec<(Rect, Rect)> = Vec::default();
    let mut textures: Vec<Texture> = Vec::default();

    for f in 0..*&file_iter.len() {
        rects.push(new_rect(&screen_size));
        textures.push(texture_creator.load_texture(&file_iter[f])?);
    }

    println!("{:#?}", rects);
    let mut player = Player {
        len: 0,
        rects,
        max_len: &file_iter.len() - 1,
        speed: PLAYER_MOVEMENT_SPEED,
        direction: Move::Stop,

        screen_size,
    };

    while run {
        for event in event_pump.poll_iter() {
            match event {
                Event::Quit { .. }
                | Event::KeyDown {
                    keycode: Some(Keycode::Escape),
                    ..
                } => {
                    run = false;
                }
                Event::MouseMotion { .. } => {}

                // Event::KeyUp + Event::KeyDown + repeat = YES
                // Event::KeyUp + repeat OR Event::KeyDown + repeat = FUCK YOU
                // Up
                Event::KeyDown {
                    keycode: Option::Some(Keycode::Up),
                    repeat: false,
                    ..
                } => {
                    player.direction = Move::Left;
                }

                // Down
                Event::KeyDown {
                    keycode: Option::Some(Keycode::Down),
                    repeat: false,
                    ..
                } => {
                    player.direction = Move::Right;
                }

                // J
                Event::KeyUp {
                    keycode: Option::Some(Keycode::J),
                    repeat: true,
                    ..
                } => {
                    player.direction = Move::Up;
                }

                Event::KeyDown {
                    keycode: Option::Some(Keycode::J),
                    repeat: true,
                    ..
                } => {
                    player.direction = Move::Up;
                }

                // K
                Event::KeyUp {
                    keycode: Option::Some(Keycode::K),
                    repeat: true,
                    ..
                } => {
                    player.direction = Move::Down;
                }
                Event::KeyDown {
                    keycode: Option::Some(Keycode::K),
                    repeat: true,
                    ..
                } => {
                    player.direction = Move::Down;
                }

                Event::KeyDown {
                    keycode: Option::Some(Keycode::F),
                    ..
                } => {}

                _ => {
                    player.direction = Move::Stop;
                }
            }

            update_player(&mut player);
        }

        render(&mut canvas, &textures, &player)?;

        ::std::thread::sleep(Duration::new(0, 1_000_000_000_u32 / 60));
    }

    Ok(())
}

pub fn new_rect(wh: &(u32, u32)) -> (Rect, Rect) {
    (Rect::new(0, 0, wh.0, wh.1), Rect::new(0, 0, wh.0, wh.1))
}

pub fn render(
    canvas: &mut WindowCanvas,
    textures: &[Texture],
    player: &Player,
) -> Result<(), String> {
    canvas.clear();

    canvas.copy_ex(
        &textures[player.len],
        Some(player.rects[player.len].0),
        Some(player.rects[player.len].1),
        0.0,
        None,
        false,
        false,
    )?;

    // texture.update()
    canvas.present();

    Ok(())
}

pub fn update_player(player: &mut Player) {
    use self::Move::*;
    match player.direction {
        Up => {
            if player.rects[player.len].1.y < (player.screen_size.1 * MAX_DISPLAY_IMG) as i32 / 2 {
                player.speed += PLAYER_MOVEMENT_SPEED;
                player.rects[player.len].1.set_y(player.speed);
            } else {
            }
        }
        Down => {
            if player.rects[player.len].1.y < (player.screen_size.1 * MAX_DISPLAY_IMG) as i32 / 2 {
                player.speed -= PLAYER_MOVEMENT_SPEED;
                player.rects[player.len].1.set_y(player.speed);
            } else {
            }
        }

        Left => {
            if player.len > 0 {
                player.len -= 1;
            } else {
            }
        }
        Right => {
            if player.len < player.max_len {
                player.len += 1;
            } else {
            }
        }
        Stop => {}
    }
}
