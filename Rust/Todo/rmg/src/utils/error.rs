pub fn exit(text: &str) -> ! {
    eprintln!("{}", text);
    std::process::exit(-1)
}
