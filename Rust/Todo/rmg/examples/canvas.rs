use sdl2::pixels::Color;
use sdl2::rect::Rect;
use sdl2::render::Canvas;
use sdl2::video::Window;

fn main() {
    let sdl_context = sdl2::init().unwrap();
    let screen = &sdl_context
        .video()
        .unwrap()
        .current_display_mode(0)
        .unwrap();

    let screen_size = (screen.w as u32, screen.h as u32);
    let video_subsystem = sdl_context.video().unwrap();
    let window = video_subsystem.window("Example", 800, 600).build().unwrap();

    // Let's create a Canvas which we will use to draw in our Window
    let mut canvas: Canvas<Window> = window
        .into_canvas()
        .present_vsync() //< this means the screen cannot
        // render faster than your display rate (usually 60Hz or 144Hz)
        .build()
        .unwrap();

    canvas.set_draw_color(Color::RGB(0, 0, 0));
    // fills the canvas with the color we set in `set_draw_color`.
    canvas.clear();

    // change the color of our drawing with a gold-color ...
    canvas.set_draw_color(Color::RGB(255, 210, 0));
    // A draw a rectangle which almost fills our window with it !
    canvas.fill_rect(Rect::new(10, 10, 780, 580));

    // However the canvas has not been updated to the window yet,
    // everything has been processed to an internal buffer,
    // but if we want our buffer to be displayed on the window,
    // we need to call `present`. We need to call this every time
    // we want to render a new frame on the window.
    canvas.present();
    // present does not "clear" the buffer, that means that
    // you have to clear it yourself before rendering again,
    // otherwise leftovers of what you've renderer before might
    // show up on the window !
    //
    // A good rule of thumb is to `clear()`, draw every texture
    // needed, and then `present()`; repeat this every new frame.
    //
}
