// ???

fn main() {
    struct Window<'text, 'w> {
        text: &'text str,
        w: &'w str,
    }

    let window = Window {
        text: "==================================",
        w: "|",
    };

    let mut k = String::new();

    for _ in 0..window.text.len() - 2 {
        k.push(' ');
    }

    let say = "hello";

    println!("{}", window.text);
    println!("{}{}{}", window.w, k, window.w);
    println!("{}{}{}", window.w, say, window.w);
    println!("{}{}{}", window.w, k, window.w);
    println!("{}", window.text);
}
