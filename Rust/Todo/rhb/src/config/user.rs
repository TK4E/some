#[derive(Debug, PartialEq)]
pub struct Profile {
    pub name: String,
    pub about: String,

    pub hp: usize,
    pub max_hp: usize,
    pub mp: usize,
    pub max_mp: usize,
    pub exp: usize,

    pub class: Class,

    pub buff: Buff,
}

#[derive(Debug, PartialEq)]
pub enum Class {
    Warrior,
    Mage,
    Healer,
    Rogue,
}

#[derive(Debug, PartialEq)]
pub struct Equipments {
    pub headgear: EquipmentMeta,
    pub body: EquipmentMeta,
    pub hand_main: EquipmentMeta,
    pub hand_off: EquipmentMeta,
}

#[derive(Debug, PartialEq)]
pub struct EquipmentMeta {
    pub name: String,
    pub gold: usize,
    pub lv: usize,
    pub buff: Buff,
}

#[derive(Debug, PartialEq)]
pub struct Pets {
    pub pet: PetMeta,
}

#[derive(Debug, PartialEq)]
pub struct PetMeta {
    pub name: String,
}

#[derive(Debug, PartialEq)]
pub struct Mounts {
    pub mount: MountMeta,
}

#[derive(Debug, PartialEq)]
pub struct MountMeta {
    pub name: String,
}

#[derive(Debug, PartialEq)]
pub struct Buff {
    pub strength: BuffBonus,
    pub intelligence: BuffBonus,
    pub constitution: BuffBonus,
    pub perception: BuffBonus,
}

#[derive(Debug, PartialEq)]
pub struct BuffBonus {
    pub gear: i8,
    pub class: i8,
}

impl Profile {
    pub fn new(name: &str, about: &str, num: &u8) -> Self {
        match rand_class(num) {
            Class::Healer => Profile {
                name: name.to_string(),
                about: about.to_string(),

                hp: 10,
                max_hp: 10,
                mp: 10,
                max_mp: 10,
                exp: 0,

                class: Class::Healer,

                buff: Buff {
                    strength: BuffBonus { gear: 0, class: 0 },
                    intelligence: BuffBonus { gear: 0, class: 0 },
                    constitution: BuffBonus { gear: 0, class: 0 },
                    perception: BuffBonus { gear: 0, class: 0 },
                },
            },
            Class::Mage => Profile {
                name: name.to_string(),
                about: about.to_string(),

                hp: 10,
                max_hp: 10,
                mp: 10,
                max_mp: 10,
                exp: 0,

                class: Class::Mage,
                buff: Buff {
                    strength: BuffBonus { gear: 0, class: 0 },
                    intelligence: BuffBonus { gear: 0, class: 0 },
                    constitution: BuffBonus { gear: 0, class: 0 },
                    perception: BuffBonus { gear: 0, class: 0 },
                },
            },
            Class::Rogue => Profile {
                name: name.to_string(),
                about: about.to_string(),

                hp: 10,
                max_hp: 10,
                mp: 0,
                max_mp: 0,
                exp: 0,

                class: Class::Rogue,
                buff: Buff {
                    strength: BuffBonus { gear: 0, class: 0 },
                    intelligence: BuffBonus { gear: 0, class: 0 },
                    constitution: BuffBonus { gear: 0, class: 0 },
                    perception: BuffBonus { gear: 0, class: 0 },
                },
            },
            Class::Warrior => Profile {
                name: name.to_string(),
                about: about.to_string(),

                hp: 1,
                max_hp: 10,
                mp: 0,
                max_mp: 0,
                exp: 0,

                class: Class::Warrior,
                buff: Buff {
                    strength: BuffBonus { gear: 0, class: 0 },
                    intelligence: BuffBonus { gear: 0, class: 0 },
                    constitution: BuffBonus { gear: 0, class: 0 },
                    perception: BuffBonus { gear: 0, class: 0 },
                },
            },
        }
    }
}

pub fn rand_class(num: &u8) -> Class {
    match num {
        0 => Class::Healer,
        1 => Class::Mage,
        2 => Class::Rogue,
        3 => Class::Warrior,

        _ => unreachable!(),
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_new() {
        let p = Profile::new("test", "test", &1);
        assert_eq!(&p.name, "test");
        assert_eq!(&p.class, &Class::Mage);
    }
}
