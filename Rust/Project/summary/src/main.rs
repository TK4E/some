use std::io::{self, Read};

fn main() {
    static t: char = '/';
    static o: &str = "    ";
    static summary: &str = "- [SUMMARY.md](SUMMARY.md)";
    let mut c: usize;
    let mut p: String;
    let mut aa: String;
    let mut bb: String;

    // Read from the buffer and convert the input to Vec<&str>
    // 读取缓冲区的内容并将其转换成 Vec<&str>
    let mut buffer = String::new();
    io::stdin().read_to_string(&mut buffer).unwrap();
    let s = buffer.split('\n').collect::<Vec<&str>>();

    // Print to file header
    // 打印到文件开头
    println!("{}", summary);

    // Discard the last line(s.len() - 1)
    // 移除最后一行
    for num in 0..s.len() - 1 {
        c = s[num].matches(t).count();
        p = String::from("-    ");

        // Append 4 spaces at the end of p
        // 在 p 的末尾追加4个空格
        for _ in 0..c {
            p.push_str(o);
        }

        // Reverse the string after concatenating the string
        // 拼接字符串后反转字符串
        aa = p.chars().rev().collect::<String>();
        bb = s[num].rsplit(t).collect::<Vec<_>>()[0].to_string();

        // Treat it as a file if the file name contains the character $
        // 如果文件名当中包含 . 这个字符  则视其为文件
        if s[num].contains('.') {
            println!("{} [{}]({})", aa, bb, s[num]);
        }
        // Else, treat it as a directory
        // 否则  视其为目录
        else {
            println!("{} [{}]()", aa, bb);
        }
    }
}
