// curl http | base64 -d | cut vemss.* | base64 -d | jq | json_parse

use std::{
    fs::File,
    io::{prelude::*, BufReader},
    path::Path,
};

fn lines_from_file(filename: impl AsRef<Path>) -> Vec<String> {
    let file = File::open(filename).expect("no such file");
    let buf = BufReader::new(file);
    buf.lines()
        .map(|l| l.expect("Could not parse line"))
        .collect::<Vec<String>>()
}

fn main() {
    let args: Vec<_> = std::env::args().collect();

    let f01 = include_bytes!("./01_log.json");
    let f02 = include_bytes!("./02_dns.json");
    let f03 = include_bytes!("./03_inbounds.json");
    let lines = lines_from_file(&args[1]);
    let f123 = format!(
        "{}\n{}\n{}",
        String::from_utf8_lossy(f01),
        String::from_utf8_lossy(f02),
        String::from_utf8_lossy(f03)
    );
    println!("{{");
    println!("{}", f123);

    let mut w: String = "".to_string();
    for line in lines {
        w = remove_first_and_last(&line).into_iter().collect::<String>();
    }

    let mut vmess: Vmess = Vmess {
        add: String::from(""),
        host: String::from(""),
        id: String::from(""),
        net: String::from(""),
        path: String::from(""),
        protocol: String::from("vmess"),
        port: 446,
    };

    for word in w.split(',').collect::<Vec<&str>>() {
        let vh = [
            "\"host",
            "\"path",
            "\"add",
            "\"port",
            "\"net",
            "\"id",
            "\"protocol",
        ];

        if word.starts_with(vh[0]) {
            vmess.host = word.split(':').collect::<Vec<_>>()[1].to_string();
        }

        if word.starts_with(vh[1]) {
            vmess.path = word.split(':').collect::<Vec<_>>()[1].to_string();
        }

        if word.starts_with(vh[2]) {
            vmess.add = word.split(':').collect::<Vec<_>>()[1].to_string();
        }

        if word.starts_with(vh[3]) {
            vmess.port = word.split(':').collect::<Vec<_>>()[1]
                .to_string()
                .parse::<u32>()
                .unwrap_or(446); //todo
        }

        if word.starts_with(vh[4]) {
            vmess.net = word.split(':').collect::<Vec<_>>()[1].to_string();
        }
        if word.starts_with(vh[5]) {
            vmess.id = word.split(':').collect::<Vec<_>>()[1].to_string();
        }
        if word.starts_with(vh[6]) {
            vmess.protocol = word.split(':').collect::<Vec<_>>()[1].to_string();
        }
    }

    let output = format!(
        "
    \"outbounds\": [
    {{
          \"protocol\": \"{protocol}\",
          \"sendThrough\": \"0.0.0.0\",
            \"settings\": {{
                \"vnext\": [
                    {{
                        \"address\": {add},
                        \"port\": {port},
                        \"users\": [
                            {{
                                \"alterId\": 1,
                                \"id\": {id},
                                \"security\": \"aes-128-gcm\"
                            }}
                        ]
                    }}
                ]
        }},
            \"streamSettings\": {{
                \"network\": {net},
                \"tlsSettings\": {{
                    \"disableSystemRoot\": false
                }},
                \"wsSettings\": {{
                    \"headers\": {{
                        \"Host\": {host}
                    }},
                    \"path\": {path}
                }},
                \"xtlsSettings\": {{
                    \"disableSystemRoot\": false
                }}
            }},
            \"tag\": \"PROXY\"
        }},
{{
            \"protocol\": \"freedom\",
            \"sendThrough\": \"0.0.0.0\",
            \"settings\": {{
                \"domainStrategy\": \"AsIs\",
                \"redirect\": \":0\"
            }},
            \"streamSettings\": {{
            }},
            \"tag\": \"DIRECT\"
        }},
        {{
            \"protocol\": \"blackhole\",
            \"sendThrough\": \"0.0.0.0\",
            \"settings\": {{
                \"response\": {{
                    \"type\": \"none\"
                }}
            }},
            \"streamSettings\": {{
            }},
            \"tag\": \"BLACKHOLE\"
        }},
        {{
            \"protocol\": \"dns\",
            \"tag\": \"dns-out\"
        }}
    ],
    \"policy\": {{
        \"system\": {{
            \"statsInboundDownlink\": true,
            \"statsInboundUplink\": true,
            \"statsOutboundDownlink\": true,
            \"statsOutboundUplink\": true
        }}
    }},
    \"routing\": {{
        \"domainMatcher\": \"mph\",
        \"domainStrategy\": \"\",
        \"rules\": [
            {{
                \"inboundTag\": [
                    \"QV2RAY_API_INBOUND\"
                ],
                \"outboundTag\": \"QV2RAY_API\",
                \"type\": \"field\"
            }},
            {{
                \"ip\": [
                    \"geoip:private\"
                ],
                \"outboundTag\": \"DIRECT\",
                \"type\": \"field\"
            }},
            {{
                \"ip\": [
                    \"geoip:cn\"
                ],
                \"outboundTag\": \"DIRECT\",
                \"type\": \"field\"
            }},
            {{
                \"domain\": [
                    \"geosite:cn\"
                ],
                \"outboundTag\": \"DIRECT\",
                \"type\": \"field\"
            }}
        ]
    }}
}}
        ",
        add = vmess.add,
        host = vmess.host,
        id = vmess.id,
        net = vmess.net,
        path = vmess.path,
        port = vmess.port,
        protocol = vmess.protocol
    );
    println!("{}", output);
}

pub struct Vmess {
    pub add: String,
    pub host: String,
    pub id: String,
    pub net: String,
    pub path: String,
    pub port: u32,
    pub protocol: String,
}

pub fn remove_first_and_last(value: &str) -> Vec<char> {
    let mut chars = value.chars();
    chars.next();
    chars.next_back();
    chars.collect::<Vec<char>>()
}
