```text
./
├── Haskell/
│  └── src/
├── Python/
│  └── Projects/
│     ├── cue_split/
│     └── get_title/
└── Rust/
   ├── Crate/
   │  ├── crossterm/
   │  │  └── src/
   │  ├── daemoni/
   │  │  ├── examples/
   │  │  └── src/
   │  ├── lexopt/
   │  │  └── src/
   │  ├── minifb/
   │  │  ├── examples/
   │  │  └── src/
   │  ├── nom/
   │  │  ├── examples/
   │  │  │  └── furigana.rs/
   │  │  └── src/
   │  ├── sqlx/
   │  │  └── src/
   │  ├── tempfile/
   │  │  └── src/
   │  ├── walkdir/
   │  │  └── src/
   │  └── xdg_dir/
   │     └── src/
   ├── Fork/
   │  ├── fbsnake/
   │  ├── hentai-downloader/
   │  │  ├── bin/
   │  │  └── src/
   │  │     └── cli/
   │  ├── math_expr_parser/
   │  │  └── src/
   │  └── rl_mandelbrot/
   │     └── src/
   ├── Learn/
   │  ├── rust_2021/
   │  │  └── src/
   │  └── src/
   │     ├── algo/
   │     ├── lib/
   │     └── lifetime_1/
   │        └── src/
   ├── Project/
   │  ├── bar/
   │  │  └── src/
   │  ├── bar_2/
   │  │  └── src/
   │  ├── get_password/
   │  │  └── src/
   │  ├── lines/
   │  │  └── src/
   │  ├── lines_2/
   │  │  └── src/
   │  ├── music_tag/
   │  │  └── src/
   │  ├── mvmore/
   │  │  └── src/
   │  ├── rand_word/
   │  │  └── src/
   │  ├── summary/
   │  │  └── src/
   │  ├── todo_list/
   │  │  └── src/
   │  └── vmess_parse/
   │     └── src/
   ├── std/
   │  ├── f64/
   │  │  └── src/
   │  └── Path/
   │     └── src/
   └── Todo/
      ├── card-tui/
      │  └── src/
      │     └── lib/
      ├── get_color/
      │  └── src/
      ├── image_to_ascii/
      │  └── src/
      ├── jisyo/
      │  └── src/
      ├── kinns/
      │  └── src/
      ├── rcn/
      │  └── src/
      ├── rhb/
      │  └── src/
      │     ├── config/
      │     └── utils/
      ├── rhs/
      ├── rmg/
      │  ├── examples/
      │  ├── src/
      │  │  ├── archive/
      │  │  └── utils/
      │  └── tests/
      │     └── files/
      ├── romaji/
      │  └── src/
      ├── RPN/
      │  └── src/
      ├── rtt/
      │  └── src/
      ├── text_game/
      │  └── src/
      └── vidir/
         └── src/
